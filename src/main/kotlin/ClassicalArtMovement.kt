class ClassicalArtMovement(): ArtMovement("Classical", "1400s-1900s") {
    private val keyArtists = listOf("Jacques-Louis David", "Benjamin West", "Peter Paul Rubens", "Jean-Honore Fragonard", "Michelangelo", "Raphael", "Leonardo da Vinci")

    fun listKeyArtists() {
        println(keyArtists.joinToString(", "))
    }
}