fun main() {
    val artMovements = listOf(ClassicalArtMovement(), ContemporaryArtMovement(), ModernArtMovement())

    while (true) {
        println("Enter the name of an art movement, or \"exit\" to quit: ")
        val input = readlnOrNull()
        if (input.equals("exit", ignoreCase = true)) break
        val foundMovement = artMovements.find { it.name.equals(input, ignoreCase = true) }
        if (foundMovement != null) {
            foundMovement.describe()
            when (foundMovement) {
                is ClassicalArtMovement -> foundMovement.listKeyArtists()
                is ContemporaryArtMovement -> foundMovement.listInfluentialWorks()
                is ModernArtMovement -> foundMovement.describeCharacteristics()
            }
        }
    }
}