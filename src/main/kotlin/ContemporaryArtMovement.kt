class ContemporaryArtMovement: ArtMovement("Contemporary", "1960-Present") {
    private val influentialWorks = listOf("Campbell's Soup Cans - Andy Warhol", "Maman - Louise Bourgeois", "The Physical Impossibility of Death in the Mind of Someone Living - Damien Hirst", "Propped - Jenny Saville", "Girl with Balloon - Banksy")

    fun listInfluentialWorks() {
        println(influentialWorks.joinToString(", "))
    }
}