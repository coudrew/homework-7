class ModernArtMovement(): ArtMovement("Modern", "1870-1960") {
    private val definingCharacteristics =
        "A movement away from realistic depictions of the world, and commissioned work, and towards art for art's sake drawing inspiration from the artist's own experience"
    fun describeCharacteristics() {
        println(definingCharacteristics)
    }
}